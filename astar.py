import sys
sys.path.insert(0, './red86')

from Value import *
from Location import *

from MemState import *
from AState import *
from Action import *

from Graph import *
from Node import *

try:
        import Queue as Q  # ver. < 3.0
except ImportError:
        import queue as Q


eax = Location(reg = "eax")
ebx = Location(reg = "ebx")
ecx = Location(reg = "ecx")
esi = Location(reg = "esi")
edi = Location(reg = "edi")

gadgets = set([XorAction(eax, eax),
               XorAction(ebx, ebx),
               XorAction(ecx, ecx),
               MoveAction(eax, Value(4)),
               MoveAction(ebx, Value(1)),
               MoveAction(ecx, Value(5)),
               AddAction(ebx, Value(1)),
               MoveAction(eax, Value(1))
])

target_instrs = [
    AddAction(eax, Value(1)),
    XorAction(eax, eax),
    XorAction(ebx, ebx),
    XorAction(ecx, ecx),
    MoveAction(eax, Value(4)),
    MoveAction(ebx, Value(1)),
    AddAction(ebx, eax),
    AddAction(ebx, eax),
    AddAction(eax, eax),
    XorAction(ebx, eax),
    MoveAction(ecx, Value.atLocation(Location(atAddr = eax))),
    LeaAction(ecx, Value.atLocation(Location(atAddr = esi))),
    MoveAction(ecx, Value.atLocation(Location(atAddr = esi))),
    MoveAction(esi, Value.atLocation(Location(atAddr = esi))),
    MoveAction(ecx, Value.atLocation(Location(atAddr = esi))),
    LeaAction(ecx, Value.atLocation(Location(atAddr = esi))),
    XorAction(eax, eax),
    MoveAction(eax, Value(1)),
    XorAction(ebx, ebx)
]


def get_dependencies(sequence):
    dependencies = []
    pass


reassignments = {}

def freshen_instructions(sequence):
    last_reass = {}
    reassigned = []
    for action in sequence:
        reass_action = action
        if action.doesOverwriteDst():
            fresh = Location.freshReg()
            new_src = action.src.reassigned(last_reass)
            last_reass[action.dst] = fresh
            reassignments[fresh] = action.dst
            new_dst = action.dst.reassigned(last_reass)
#        return self.__class__(new_dst, new_src)
#            reass_action = reass_action.reassigned(last_reass)
            reass_action = action.__class__(new_dst, new_src)
        elif action.dst in last_reass:
            reass_action = reass_action.reassigned(last_reass)
        if action.dst != action.src and action.src in last_reass :
            reass_action = reass_action.reassigned(last_reass)
        reassigned.append(reass_action)
        print("")
        print(action)
        print(action.equivalence(reass_action))
        print(sequence[2].equivalence(action))
        print(reass_action)
    return reassigned

#reassigned = freshen_instructions(target_instrs)

print("")
for i in reassignments:
    print(str(i))


print("\n\n\nttttttttttttttttttttttttttttttttttttttt0")
graph = Graph.from_file("input.txt")
print("\n\n\nttttttttttttttttttttttttttttttttttttttt1")
print(graph)
graph.freshen()
print(graph)
print(reassignments)
print("\n\n\nttttttttttttttttttttttttttttttttttttttt2")
graph.print_dependencies()
print("\n\n\nttttttttttttttttttttttttttttttttttttttt3")



class AStar:
    def __init__(self, graph, gadgets):
        self.graph = graph
        self.gadgets = gadgets

    def run(self):
        print("start astar")

        state = AState()

        """

        need set of available actions for given state
        need heuristic for state

        A state must know its assignment from r* to e**

        if same action or same state (i.e. with current assignment), append with h 100.
        Otherwise :
        if equivalent action, append reassignment action with small h. We want a (<4)-step solution more than a reassignment
        if equivalent states, same
        otherwise if no equivalence :
        if dst is same: big h
        if dst is equivalent: reassignment

        """

    def step(self, curr_state, target_state, ideal_gadget):
        frontier = Q.PriorityQueue()
        while(True):
            for g in gadgets:
                next_state = g.apply(curr_state)

                h = 1
                eq = g.equivalence(ideal_gadget)
                print(g, ideal_gadget, eq)
                if eq == {}:
                    h = 100
                elif eq != None:
                    dict = {}
                    for (a1, b1) in eq:
                        for (a2, b2) in eq:
                            if (b1 == b2 and a1 != a2):
                                h = 2
                                dict = None
                        if dict != None:
                            dict[a1] = b1
                        else:
                            break
                    if dict != None:
                        next_state = next_state.reassigned(dict)
                        print(curr_state, g, next_state)
                        h = 50

                if next_state == curr_state:
                    h = 0

                frontier.put((-h, next_state, g))


            (h, best_state, best_gadget) = frontier.get()
            print("best state found with h " + str(h) + " was " + str(best_state) + " with action " + str(best_gadget))

            if best_gadget.equivalence(ideal_gadget) != None:
                print("done!")
                break



target = AddAction(Location(reg = "r42"), Value(2))
s = target.apply(MemState())
print("asking for AStar to do single step from state " + str(MemState()) + " to state " + str(s) + " with an ideal gadget " + str(target_instrs[0]))
AStar(graph, gadgets).step(MemState(), s, target)



"""

Need to give it a goal, like the sequence of states it has to go through.

Should construct the sequence of states first and that way create the states it has to go through, plus the instructions that got it there, so that if it finds the instruction corresponding, it doesn't have to compute the action ?

Or maybe, first pass try to find all the instructions/actions that we have as is, then


Existing constraints on the solution:
 - read/write dependencies
 - (final register assignment (if we do register re-organization))
 - states
 - register coherence (can switch registers only if completely new value)

Variables to give an assignment:
 - Assign a register to each "atomic reg"
 - Assign a gadget to each state transition (is that a variable? (I guess))

I REALLY need a way to represent these registers.
Actually, I would even have to, at start, give uids to each register that cannot be split into two, meaning it never gets reset to an entirely new value, and only gets mutated relative to its previous value.
These uids would represent atomic regs.
Astar would have this as a variable.
So Astar would, at each step, see if it can assign a reg, make an action. Assigning a reg should be done as late as possible to avoid backtracking too much in case of a dead end for a given assignment. For choosing the action, first try and find a gadget that is equivalent (with the current reg assignments) to the provided instruction that led to the target state, otherwise find the one that changes the minimum number of reg values.





a target state must only have r* registers
a state generated from astar must have no r* registers.

AStar is actually run once per instruction given by the user. This means that if there are 10 instructions, Astar will be run 10 times, starting with an empty frontier and everything each time.
The target for each astar step will be a state with r* registers for those that are reassignable, and e** for the others. It will also be given an ideal gadget for help, which will follow the same r*/e** policy.
Each AStar's initial state must also follow this r*/e** policy. For the very first call to AStar, the caller might have to call it multiple times with different assignments.
Astar can only make the assignment more specific. It is not allowed to modify an already assigned r* register.
AStar will try its best to avoid reassignments:
    - It will first try to find the ideal gadget.
    - It will then try to find a gadget that ends up in the same state (is this equivalent to the previous one?)
    - It will then try a gadget that modifies the same register. Probably BFS among them, unless I can come up with a heuristing for which instruction to choose (xor, mov, add, etc). It will differentiate actions that overwrite dst and those that don't.
    - It will then finally allow itself to reassign registers that can be reassigned (all might not be, if they have been decided in a previous state).
    - Finally, if none is available, it will give up.

If AStar gives up, the caller knows that no gadget and no assignment can lead to the next state. It must hence either change an assignment decision it made and try again, or fail as well and backtrack to the previous state to try a different assignment.





If I want to have one big AStar, which is highly preferable:
AStar has a frontier
States keep track of the current r*/e** assignments, in order to know which e** have not yet been assigned. This also includes freeing registers e** whose assigned r* registers will never be used again in the future, making them available for future reassignments to new r* registers.
States keep track of their previous state
States keep track of the remaining graph, in which no state has been reached yet. This graph will have multiple roots that would be the states that have no more dependency constraints and could therefore be tried next.
AStar keeps track of a current state, which will use e** registers for already assigned ones, and r* for those that can still be assigned.
At each iteration, there might be multiple target states, depending on dependencies. Maybe find a heuristic for which to try first.
At each iteration, for each allowed target state and, optionally, the associated ideal gadget:
  - It will, for each gadget, add to the frontier with decreasing priority:
    - The ideal gadget, if available
    - Gadgets that end up in the target state
    - Gadgets that modify the same register with the same dst-overwrite characteristic
  - With lower priority, it will also add to the frontier
    - Reassignents of allowed registers
There should be a security that avoids infinite xor eax, eax and so on.
When a state is created and added to the frontier, it must be given its previous state, and an updated graph, which must take into account potential dependency updates caused by reassignments or state insertions.






"""
