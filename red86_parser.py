import sys
from enum import Enum
from pwn import *

if len(sys.argv) != 3 :
    print("This programs takes two arguments : <binary_file> and <shellcode_file>")
    exit(0)

binary = sys.argv[1]
shellcode = sys.argv[2]

context.binary = "./"+binary


# TODO allow to interleave stuff on the stack (like "/bin//sh")
# TODO might be possible to make Reg the enum directly
# TODO implement regex
# TODO implement choice for hex representation of imm
class Reg(Enum):
    EAX = 1
    EBX = 2
    ECX = 3
    ESI = 4
    EDI = 5

    def dependent_regs(self) :
        return [self]

    def getAllRegs(self):
        return [self]

    def toString(self, regex = False) :
        return self.name.lower()
    def __repr__(self):
        return self.toString()
#    def __eq__(self, other):
#        if isinstance(self, other.__class__):
#            return self == other
#        return False
#    def __ne__(self, other):
#        return not self.__eq__(other)
#    def __hash__(self):
#        return self.value

class Imm:
    def __init__(self, imm):
        self.imm = imm
    def dependent_regs(self):
        return []
    def toString(self, regex = False) :
        return str(self.imm)
    def __repr__(self):
        return self.toString()
    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return self.imm == other.imm
        return False
    def __ne__(self, other):
        return not self.__eq__(other)
    def __hash__(self):
        return self.imm

class Mem:
    def __init__(self, base, index = None, scale = 1, displacement = None):
        if not scale in [0, 1, 2, 4, 8] :
            print ("ERROR : scale factor must be a power of two smaller than 9")
        self.base = base
        self.index = index
        self.scale = scale
        self.displacement = displacement

    def dependent_regs(self):
        regs = [self.base]
        if self.index != None:
            regs.append(self.index)
        return regs

    def getAllRegs(self):
        regs = [self.base]
        if self.index != None:
            regs.append(self.index)
        return regs

    def remap(self, reg1, withreg2):
        if self.base == reg1:
            self.base = withreg2
        elif self.base == withreg2:
            self.base = reg1
        if self.index != None:
            if self.index == reg1:
                self.index = withreg2
            elif self.index == withreg2:
                self.index = reg1

    def toString(self, regex = False) :
        s = "["+self.base.toString(regex)
        if self.index != None and self.scale != 0:
            s += " + " + self.index.toString(regex)
            if self.scale != 1:
                s += " * " + str(self.scale)
        if self.displacement != None:
            s += " + "+self.displacement.toString(regex)
        s += "]"
        return s
    def __repr__(self):
        return self.toString()


# TODO return opcode representation of the thing ?
class Instr:

    instructions = ["int",
                    "xor",
                    "mov",
                    "add",
                    "sub",
                    "lea"]

    def __init__(self, op, a1, a2 = None):
        op = op.lower()
        if op == "int":
            if a2 != None: # TODO test types of arguments
                print("ERROR : int takes only one argument.")
            if not isinstance(a1, Imm):
                print("ERROR : int takes an immediate as first argument.")
            self.__instr = Instr.__Int(a1)
        elif op in Instr.instructions:
            if not (isinstance(a1, Reg) or isinstance(a1, Mem)) :
                print("ERROR : "+op+" takes either a register or a memory address as first argument.")
            if op == "lea":
                if not (isinstance(a2, Mem)) :
                    print("ERROR : lea takes a pointer to memory as its second argument.")
            elif not (isinstance(a2, Reg) or isinstance(a2, Mem)) :
                print("ERROR : "+op+" takes either a register or a memory address as second argument.")
            self.__instr = Instr.__Instr(op, a1, a2)
        else:
            self.__instr = None
            print("ERROR : Unknown instruction " + op)
        self.__opcode = asm(str(self.__instr.toString()))

    class __Int:
        def __init__(self, imm):
            self.__imm = imm

        def dependencies(self):
            return {"reg_r" : set(),
                    "reg_w" : set(),
                    "mem_r" : False,
                    "mem_w" : False,
                    "barrier" : True}


        def toString(self, regex = False):
            return "int " + self.__imm.toString()

    class __Instr:
        def __init__(self, op, dst, src):
            self.__op = op
            self.__dst = dst
            self.__src = src

        def dependencies(self):
            dict = {"reg_r" : set(),
                    "reg_w" : set(),
                    "mem_r" : False,
                    "mem_w" : False,
                    "barrier" : False}

            dict["reg_r"].update(self.__src.dependent_regs())
            if isinstance(self.__dst, Mem):
                dict["reg_r"].update(self.__dst.dependent_regs())
                dict["mem_w"] = True
            else:
                dict["reg_w"].update(self.__dst.dependent_regs())
            if isinstance(self.__src, Mem):
                dict["mem_r"] = True
            return dict

        def getAllRegs(self):
            regs = []
            regs += self.__dst.getAllRegs()
            regs += self.__src.getAllRegs()

        def remap(self, reg1, withreg2):
            if isinstance(self.__dst, Reg):
                if self.__dst == reg1:
                    self.__dst = withreg2
                elif self.__dst == withreg2:
                    self.__dst = reg1
            else:
                self.__dst.remap(reg1, withreg2)
            if isinstance(self.__src, Reg):
                if self.__src == reg1:
                    self.__src = withreg2
                elif self.__src == withreg2:
                    self.__src = reg1
            elif self.__src != None:
                self.__src.remap(reg1, withreg2)

        def toString(self, regex = False):
            return self.__op.lower() + " " + self.__dst.toString() + ", " + self.__src.toString()

    def getAllRegs(self):
        if isinstance(self.__instr, Instr.__Instr):
            self.__instr.getAllRegs()
        else:
            return []

    def remap(self, reg1, withreg2):
        if isinstance(self.__instr, Instr.__Instr):
            self.__instr.remap(reg1, withreg2)

    def dependencies(self):
        return self.__instr.dependencies()

    def toString(self, regex = False):
        return self.__instr.toString()

    def getOpCode(self):
        return self.__opcode

    def __repr__(self):
        return self.toString()


# TODO allow parsing hex values
# TODO allow displacement to be negative
def parseLine(s):
    s = s.strip()
    op = s.split(' ')[0]
    token = ""
    in_mem = False
    in_mem_phase = 0
    base = None
    index = None
    scale = 1
    displacement = None
    args = []
    for c in s[3:]:
        if c == ' ' or c == ']' or c == '\t':
            if in_mem and len(token) != 0:
                if in_mem_phase == 0:
                    base = Reg[token.upper()]
                elif in_mem_phase == 1:
                    try:
                        displacement = Imm(int(token))
                        in_mem_phase = 3
                    except:
                        index = Reg[token.upper()]
                elif in_mem_phase == 2:
                    scale = int(token)
                elif in_mem_phase == 3:
                    displacement = Imm(int(token))
            if in_mem and c == ']':
                args.append(Mem(base, index, scale, displacement))
                in_mem = False
                in_mem_phase = 0
                displacement = None
                scale = 1
                index = None
                base = None
            token = ""
        elif c == '[' and not in_mem:
            in_mem = True
        elif c == ',' and not in_mem:
            if len(token) != 0:
                try:
                    args.append(Imm(int(token)))
                except:
                    args.append(Reg[token.upper()])
        elif (c == '*' or c == '+') and in_mem:
            if not (c == '*' and in_mem_phase == 1) and not (c == '+' and (in_mem_phase == 0 or in_mem_phase == 2)):
               print("ERROR : Incorrect operator : " + str(c))
               return None
            in_mem_phase += 1
        else:
            token += c
    if len(token) != 0:
        try:
            args.append(Imm(int(token)))
        except:
            args.append(Reg[token.upper()])

    if len(args) > 2 or len(args) < 1:
        print("ERROR : Wrong number of arguments")
        return None

    args.append(None)

    return Instr(op, args[0], args[1])


class Node:
    def __init__(self, instr):
        self.instr = instr
        self.aft = set()
        self.bef = set()

    def isBef(self, other):
        if isinstance(other, Node):
            self.aft.add(other)
            other.bef.add(self)
        else:
            self.aft.update(other)
            for o in other:
                o.bef.add(self)

    def isAft(self, other):
        if isinstance(other, Node):
            self.bef.add(other)
            other.aft.add(self)
        else:
            self.bef.update(other)
            for o in other:
                o.aft.add(self)

class Graph:
    def __init__(self):
        self.__nodes = set()
        self.__root = None
        self.__count = 0
        self.assign = {}
        for name, r in Reg.__members__.items():
            self.assign[r] = r
    def addNode(self, node):
        if self.__root == None:
            self.__root = node
        if not (node in self.__nodes): # TODO might remove this ?
            self.__nodes.add(node)
            self.__count += 1
    def isEmpty(self):
        return self.__conut == 0

    def print_dependencies(self):
        for n in self.__nodes:
            print("node : " + str(n.instr) + "\n  must be before : ")
            for n2 in n.aft:
                print("\t"+str(n2.instr))
            print("  and must be after : ")
            for n2 in n.bef:
                print("\t"+str(n2.instr))
    def print_opcodes(self):
        for n in self.__nodes:
            print(str(n.instr))
            print(asm(str(n.instr)).encode('hex'))
    def getNodes(self):
        return self.__nodes



# print(Reg.EAX.toString())
# print(Mem(Reg.EAX).toString())
# print(Mem(Reg.EAX, Reg.EBX).toString())
# print(Mem(Reg.EAX, Reg.EBX, scale = 2).toString())
# print(Mem(Reg.EAX, Reg.EBX, scale = 1, displacement = Imm(1)).toString())
# print(Mem(Reg.EAX, displacement = Imm(2)).toString())
#
# instr = Instr("xor", Mem(Reg.EAX, displacement = Imm(2)), Reg.EBX)
# print(instr.toString())
# instr.remap(Reg.EAX, Reg.EBX)
# print(instr.toString())
# print(Instr("lea", Mem(Reg(Regs.EAX), displacement = Imm(2)), Reg(Regs.EAX)).toString())
# print(Instr("int", Imm(2)).toString())

"""
============ BUILDING GRAPH ============
"""
graph = Graph()
with open(shellcode) as f:
    nodes = []
    for line in f.readlines():
        if len(line) > 0 and line[0] == '#':
            continue
        instr = parseLine(line)
        if (instr == None) :
            print("Impossible to parse line " + line + ". skipping it.")
            continue
        dep = instr.dependencies()
        node = Node(instr)
        nodes.append((node, dep))
        graph.addNode(node)
    node_count = len(nodes)
    for i, (node, dep) in enumerate(nodes):
        if dep["barrier"]:
            for node2, _ in nodes[i+1:]:
                node.isBef(node2)
            for node2, _ in nodes[:i]:
                node.isAft(node2)
            continue
        if i < node_count - 1:
            for (node2, dep2) in nodes[i+1:]:
                is_dep = False
                for d in dep["reg_r"]:
                    if d in dep2["reg_w"]:
                        is_dep = True
                        break
                for d in dep["reg_w"]:
                    if d in dep2["reg_r"] or d in dep2["reg_w"]:
                        is_dep = True
                        break
                is_dep = is_dep or (dep["mem_r"] and dep2["mem_w"]) or (dep["mem_w"] and dep2["mem_r"]) or (dep["mem_w"] and dep2["mem_w"])
                if is_dep:
                    node.isBef(node2)

graph.print_dependencies()
graph.print_opcodes()


"""
============ EXTRACTING FROM BINARY ============
"""
# Will output a set of addresses corresponding to instructions we want,
# along with the assignment of variables
def extract_binary() :
    found_int = False
    window_size = 15
    found = []
    window = ['\x90'] * window_size
    with open(binary) as f:
        c = f.read(1)
        while c:
            window.append(c)
            if (not found_int) and window[-2] == '\xcd' and window[-1] == '\x80':
                print("//////////////////////////////")
                found_int = True
            del window[0]
            if c == "\xc3":
                s = ""
                for c in window:
                    s += c
                found.append(s)
            c = f.read(1)
    print(len(found))
    for i, f in enumerate(found):
        if i > 20:
            break
        print(f.encode('hex'))
    return found



"""
============ FINDING INSTRUCTIONS FROM EXTRACTION ============
"""
extracted = extract_binary()
done = False
found = []
for i, e in enumerate(extracted):
    for n in graph.getNodes():
        instr = n.instr
        opcode = instr.getOpCode()
        if opcode in e and not instr in found:
            index = e.find(opcode)
            print(e[index:].encode('hex'))
            print(disasm(e[index:]))
            print(e.encode('hex'))
            print(instr)
            print(opcode.encode('hex'))
            found.append(instr)
            done = True
            break
    if done:
        pass
#        break



#while (True) :
#    line = raw_input(">>> ")
#    instr = parseLine(line)
#    print(instr)
#    print(instr.dependencies())

# print(Reg(Regs.EAX).toString())
# print(Mem(Reg(Regs.EAX)).toString())
# print(Mem(Reg(Regs.EAX), Reg(Regs.EBX)).toString())
# print(Mem(Reg(Regs.EAX), Reg(Regs.EBX), scale = 2).toString())
# print(Mem(Reg(Regs.EAX), Reg(Regs.EBX), scale = 1, displacement = Imm(1)).toString())
# print(Mem(Reg(Regs.EAX), displacement = Imm(2)).toString())

# print(Instr("xor", Mem(Reg(Regs.EAX), displacement = Imm(2)), Mem(Reg(Regs.EAX), displacement = Imm(2))).toString())
# print(Instr("lea", Mem(Reg(Regs.EAX), displacement = Imm(2)), Reg(Regs.EAX)).toString())
# print(Instr("int", Imm(2)).toString())


"""

To find the gadgets :
 - I look only for those that I need, without register reassignments.
 - I look for the operations I need, for all register assignments possible and take the assignment that finds the most gadgets.
 - I look for the operations I need, regardless of the registers used in the binary. For this I would need to be able to recognize the operation regardless of the registers. Previous solution is probably better
 - I take all ret-terminated gadgets, then keep only those that I know how to parse and use those as gadgets. Basically improved version of the previous one.


Using A* :
The problem is that I will most likely not be able to find the gadgets that I want, even with register reassignments. In this case I would need to be able to use other gadgets that take different routes to arrive at the same outcome.
For this I need a state representation (i.e. the values in each register). Do I still want to keep allowing the use of memory addresses if that's the case theugh? Because it would mean having to represent the state fo the memory as well... It definitely is possible, but let's say no for now.
So the state is simply the content of each register.
I would need, for each instruction, to basically be able to interpret it and know how it changes the current state. Shouldn't be too hard. This gives me a transition function. The transition function would just be : set/add to value in reg/memory address.
Now for the evaluation function... First, do we want to go through all the target states or not. Ideally, some states from the original code are just transitional states, so I should be allowed to not go through that state as long as the important states are taken. Would this mean asking the user for which states are transitional and which are not? I don't like that. Is there a way to determine this? I gues the only place where the state must be the one we want is when calling `int`. The rest is just setup basically. But if I don't care about any state except the last one, might just ask the user for the final state and generate everything else myself. Is that too hard though? I think it is.

Do I allow to skip states though ?

A problem with allowing to add an intermediary state to the sequenec of states if we can't find a direct gadget to go from one state to the next, is that there is no way to choose which intermediary state is better. There is no good evaluation function.

I would also have so many actions possible in my transition function, so I would need a clever way to choose which to try first, as well as how to store them efficiently in order to be able to access them quickly, depending on how I want to access them.
I would store them all in a set or something, and then have multiple other lists/sets containing each like "all actions mutating eax", "all actions using eax"

"""


